# OpenProject Calendar Plugin

This plugin integrates caldav calendar support into openproject. 

## Features

* caldav calendar
    * global
    * groups
    * users
    * projects
* caldav events
    * global
    * groups
    * users
    * projects
    * work packages
* caldav views
    * root
        * calendar
            > summerizes global & groups calendars as calendar sheet.
        * agenda
            > summerizes global & groups calendars as detailed list.
    * embed
        * calendar
            > summerizes either a user or project calendar as calendar sheet.
        * agenda
            > summerizes either a user, project or work package calendar as detailed list.
        * event
            > summerizes an calendar event

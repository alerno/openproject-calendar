# encoding: UTF-8
$:.push File.expand_path("../lib", __FILE__)

require 'open_project/calendar/version'
# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "openproject-calendar"
  s.version     = OpenProject::Calendar::VERSION
  s.authors     = "alerno GmbH"
  s.email       = "info@alerno.de"
  s.homepage    = "https://bitbucket.org/alerno/openproject-calendar"
  s.summary     = 'OpenProject Calendar'
  s.description = "Integrates caldav calendars into openproject."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*"] + %w(CHANGELOG.md README.md)

  s.add_dependency "rails", "~> 5.0"
end
